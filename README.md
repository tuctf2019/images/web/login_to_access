# Login to Access -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/login_to_access)

## Chal Info

Desc: `If at first you don't login, maybe back up.`

Hints:

* What happens when someone edits dynamic content?

Flag: `TUCTF{b4ckup5_0f_php?_1t5_m0r3_c0mm0n_th4n_y0u_th1nk}`

## Dependencies

This relies on a MySQL database. Currently the creds are hardcoded, so there needs to be a database called `challenge` with the root password as `funnewpasswordtimes`. In the database, there should be a `users` table with an entry for a user called `dave` with password `hunter2`:

```sql
CREATE TABLE users (user VARCHAR(20) NOT NULL, password VARCHAR(50) NOT NULL);
INSERT INTO users VALUES ('dave', 'hunter2');
```

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/login_to_access)

Ports: 80

Environment Variables:

* MYSQL_ADDRESS: The address of the MYSQL server

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 -e MYSQL_ADDRESS="127.0.0.1" asciioverflow/login_to_access:tuctf2019
```
